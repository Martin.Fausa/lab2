package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {
    // plasserer feltvariablene her slik at de initieres hver gang objektene påkaller en metode som benuytter seg av disse.
    String name;
    int healthPoints;
    int maxHealthPoints;
    int strength;
    Random random;

    public Pokemon(String nameInput) {
        // bruker this.~~ for at objektet skal hente ut sine feltvariabler.
        this.name = nameInput;
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));
    }

    public String getName() {
        // Returnerer navn til pokemon.
        return this.name;
    }

    @Override
    public int getStrength() {
        // Returnerer styrken til pokemon.
        return this.strength;
    }

    @Override
    public int getCurrentHP() {
        // Returnerer nåværende HP til pokemon.
        return this.healthPoints;
    }

    @Override
    public int getMaxHP() {
        // Returnerer størst mulige HP til pokemon.
        return this.maxHealthPoints;
    }

    public boolean isAlive() {
        // Sjekker om pokemonen fortsatt er i livet ved å se om HP er større enn 0.
        if (this.healthPoints > 0) {
            return true;
        } else{
            return false;
        }
    }

    @Override
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());  
        
        if (this.healthPoints > 0){
            // Pokemon-objekt angriper target-objekt hvis den selv lever.
            System.out.printf("%s attacks %s.\n", this.name, target.getName());
            // Kaller på metoden damage til target-objektet, og får den til å registrere skaden.
            target.damage(damageInflicted);
        }
        // Sjekker HP fra target-objektet og printer ut en tekst dersom den er beseiret.
        if (target.getCurrentHP() == 0){
            System.out.printf("%s is defeated by %s.\n", target.getName() , this.name);
        }
    }

    @Override
    public void damage(int damageTaken) {
        // Hvis skaden er et negativt tall gjør vi den om til en 0 isteden slik at den ikke gjør negativ skade.
        if (damageTaken < 0){
            damageTaken = 0;
        } 
        // Lager if-vilkår som trekker fra skaden fra sin HP, og sjekker om skaden ikke gjør mer skade enn mulig. Om skaden er større enn HP, så vil HP stoppe på 0.
        if (this.healthPoints - damageTaken >= 0){
            this.healthPoints -= damageTaken;
            System.out.printf("%s takes %d damage and is left with %d/%d HP\n", this.name, damageTaken, this.healthPoints, this.maxHealthPoints);
        } else if (this.healthPoints - damageTaken < 0){
            this.healthPoints = 0;
            System.out.printf("%s takes %d damage and is left with %d/%d HP\n", this.name, damageTaken, this.healthPoints, this.maxHealthPoints);
        }     
    }

    @Override
    public String toString() {
        // lager et string-format som gir oss en lesbar tekst sammen med feltvariablene.
        String str1 = String.format("%s HP: (%d/%d) STR:  %d", this.name, this.healthPoints, this.maxHealthPoints, this.strength);
        return str1;
    }

}
