package INF101.lab2;

import INF101.lab2.pokemon.IPokemon;
import INF101.lab2.pokemon.Pokemon;

public class Main {

    public static IPokemon pokemon1;
    public static IPokemon pokemon2;
    public static void main(String[] args) {
        // Have two pokemon fight until one is defeated
        // Initierer pokemon-objektene.
        Main.pokemon1 = new Pokemon("Pikachu");
        Main.pokemon2 = new Pokemon("Eevee");

        // Skriver ut pokemon stats.
        System.out.println(pokemon1.toString());
        System.out.println(pokemon2.toString());
        System.out.println();

        //Lager så en while-løkke som lar hver pokemon angripe hverandre helt til en av dem dør først.
        while (pokemon1.isAlive() && pokemon2.isAlive()){
            pokemon1.attack(pokemon2);
            pokemon2.attack(pokemon1);
        }


    }
}
